use futures::executor::block_on;
use std::io;
///! Demo utility for demonstrating Tarp, a canvas crate library
///! Theo Rowlett
///! CS-410P Winter '22
use tarp::{Assignments, Canvas, Course};

fn main() {
    let canvas = get_user_info();
    keep_asking(&canvas);
}

/// Basic function to continue to loop and ask for more classes until the user is done.
/// will only stop looping if user enters n.
fn keep_asking(canvas: &Canvas) {
    let mut input = String::new();
    loop {
        let course: Course = block_on(get_course(&canvas));
        println!("{}", course);
        let assignments = block_on(get_assignments(&canvas, &course));
        println!("{}", assignments);
        println!("Would you like to lookup another class? (y/n)");
        io::stdin()
            .read_line(&mut input)
            .expect("Unable to read input.");
        let check_val = input.trim();
        match check_val {
            "n" => break,
            _ => continue,
        }
    }
}

/// Gets access token key from user.
pub fn get_user_info() -> Canvas {
    let mut input = String::new();
    println!("For the purposes of this demo you will need an access token. A full implementation of this would require the developer to use a token and perform OAUTH login.");
    println!("Since this is purely for educational purposes, please generate a token at: https://canvas.pdx.edu/profile/settings (note: This demo assumes that the user has a PSU Canvas login.");
    println!("Enter the key below:");
    io::stdin()
        .read_line(&mut input)
        .expect("Unable to read input.");
    Canvas::new(None, Some(input))
}

/// Gets course info. Will panic if the user enters anything that isn't able to parse to a usize.
pub async fn get_course(canvas: &Canvas) -> Course {
    let mut input = String::new();
    println!("Enter the course ID to lookup");
    io::stdin()
        .read_line(&mut input)
        .expect("Unable to read input.");
    let course_id: usize = input.trim().parse().unwrap();
    let course: Course = Course::new(
        canvas.url().to_string(),
        course_id,
        canvas.token().to_string(),
    )
    .await;
    course
}

/// Gets assignments for a specific class.
pub async fn get_assignments(canvas: &Canvas, course: &Course) -> Assignments {
    Assignments::new(
        canvas.url().to_string(),
        course.id,
        canvas.token().to_string(),
    )
    .await
}
