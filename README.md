# Tarp - A Rust Crate for interfacing with the Canvas LMS API

---

### Authors

Theo Rowlett 2022 - trowlett@pdx.edu

---

### About

##### Library

Tarp uses the [Canvas LMS REST API](https://canvas.instructure.com/doc/api/) to allow the creation of Cavnas applications and websites that can interface with a schools installation of Canvas.  This crate will support aggregating data from the most popular endpoints available within the Canvas API, including users, classes, grades, assignments, etc, into structs that will be readily useable for other binary applications.

##### Demo

A demo of the crates application will be created, which will be a command line interface to display a list of a user's total assignments for all classes currently enrolled. This application will only serve to demonstrate the practical application of the crate in question.

---

### Initial Concerns

- Inaccesible Enpoints - Although getting an API key is a non-issue for Portland State's Canvas installation, some endpoints will be inaccessible for testing due to my status as a student. The scale of this crate will most likely be limited in scope to endpoints that are accessible (ie. getting grades will be possible but grading assignments will not)

- Parsing JSON data - Although I am familiar with formatting JSON data in Python, this will be a new adventure within Rust. I'm confident that there will be crates available to assist with this.

- Demo single page webapp - I will have to learn a framework to make all of this work. Fortunately there are several frameworks available, I will just need to choose one.

---

### Development Milestones

- [x] Decide what libaries are going to be needed for:
  
  - [ ]  ~~Web Framework (Demo)~~
  
  - [x]  REST (Library)

- [x]  Make decision how API Key will be accessed that will allow it to remain obfuscated (need to read more about this prior to implementing)

- [x] Access user enpoints

- [x] Access class endpoints

- [x] create methods for:
  
  - [x] returning class data
  
  - [x] returning assignment data
  
  - [ ] ~~returning grade data~~ Need OAuth login to test. Scrapping for this release.

- [ ] ~~Demo single page app~~

- [x]  Demo CLI application

---

### Build and Run

```cargo run``` for demo, ```cargo build``` for library in associated directories.

This release requires a authorization token to be aquired by the user. This is for demonstration purposes only, accessing canvas in this manner is against it's TOS. If this were to be implemented upon further, proper OAUTH token handling support would have to be implemented. A token can eb aquired at [the Canvas user settings page](https://canvas.pdx.edu/profile/settings). Demoing the application will require knowing of any class id's of any classes they want to look up.

---

### Reflection

I definitely underestimated the difficulty of parsing JSON data in Rust. The crates to do so are definitely not as mature as to what I was used to in Python. Particuarly reading into a struct that is a vector of other objects, I'm looking at you assignments. But overall this was fun, and I definitely learned a lot, not only about parsing data and rust, but logins and security. I was upset that I couldn't get an OAuth2 login working, but without the credentials it is impossible (and the University wouldn't want to give a student access to everything). All of the data parsing did get me exposure to lots of different crates, reading through the documentation for said crates, and implementing that I wouldn't have done without this project. I definitely appreciates the cargo system more, much better than pip or any other package manager I've ever used.

Looking back there are some things I would do differently. Having API calls return Results rather than just the object in question would be the number one thing I would change. Instead I had the match call from within the new() fn, and created an empty struct to return. Not the best implementation, but it's better than just having it panic.