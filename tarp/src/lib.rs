use ::std::time::Duration;
use reqwest::Error;
use serde::Deserialize;
///! Library for interfacing and retrieving data from Canvas LMS.
///! Theo Rowlett
///! CS-410P Winter '22
use std::fmt::{self};

pub struct Canvas {
    pub url: String,
    pub access_token: String,
}

/// Canvas takes 2 Option arguments, this was a result of speeding up testing.
/// If this actually is used, can be useful to call the token rather than the client.
/// To use any of the test methods below, a functioning key will need to be added on line 24.
impl Canvas {
    pub fn new(canvas_url: Option<String>, token: Option<String>) -> Canvas {
        let arg_url: String = match canvas_url {
            None => "https://canvas.pdx.edu/".to_string(),
            Some(_) => canvas_url.unwrap(),
        };
        let arg_token: String = match token {
            None => "[DEFAULT_KEY_GOES_HERE]".to_string(),
            Some(_) => token.unwrap(),
        };
        Canvas {
            url: arg_url,
            access_token: arg_token,
        }
    }

    pub fn url(&self) -> &String {
        &self.url
    }

    pub fn token(&self) -> &String {
        &self.access_token
    }
}

#[derive(Deserialize, Debug)]
pub struct Course {
    pub id: usize,
    pub name: String,
    pub account_id: usize,
    pub uuid: String,
    pub start_at: String,
    pub created_at: String,
    pub course_code: String,
}
/// All code for handling of course objects. If the course id cannot find an associated course, the new()
/// function with return an empty course with only the course number.
impl Course {
    pub async fn new(canvas_url: String, course_id: usize, api_token: String) -> Course {
        let url = format!(
            "{}api/v1/courses/{}?access_token={}",
            canvas_url, course_id, api_token
        );
        let course = Course::get_course_info(url);
        match course {
            Ok(_) => course.unwrap(),
            Err(..) => Course {
                id: course_id,
                name: "Not Found".to_string(),
                account_id: 0,
                uuid: "N/A".to_string(),
                start_at: "N/A".to_string(),
                created_at: "N/A".to_string(),
                course_code: "N/A".to_string(),
            },
        }
    }
    #[tokio::main]
    pub async fn get_course_info(url: String) -> Result<Course, Error> {
        let client = reqwest::Client::new();
        let course: Course = client
            .get(url)
            .timeout(Duration::from_secs(3))
            .send()
            .await?
            .json()
            .await?;
        Ok(course)
    }
}
/// Display impl for Course, to make output look nicer.
impl fmt::Display for Course {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"Course ID: {num}\nCourse Name: {name}\nAccount ID: {account_id}\nUUID: {uuid}\nStart At: {start}\nCreated at:{created}\nCourse Code:{code}\n",
        num = self.id,
        name = self.name,
        account_id = self.account_id,
        uuid = self.uuid,
        start = self.start_at,
        created = self.created_at,
        code = self.course_code)
    }
}

#[derive(Deserialize, Debug)]
pub struct Assignment {
    pub id: usize,
    pub description: String,
    pub due_at: String,
    pub created_at: String,
    pub updated_at: String,
}
/// Assignment impl, will return an empty assignment object if the assignment number cannot be found.
impl Assignment {
    pub async fn new(
        canvas_url: String,
        course_id: usize,
        assignment_id: usize,
        api_token: String,
    ) -> Assignment {
        let url = format!(
            "{}api/v1/courses/{}/assignments/{}?access_token={}",
            canvas_url, course_id, assignment_id, api_token
        );
        let assignment = Assignment::get_assignment_info(url);
        match assignment {
            Ok(_) => assignment.unwrap(),
            Err(..) => Assignment {
                id: assignment_id,
                description: "Not Found".to_string(),
                due_at: "N/A".to_string(),
                created_at: "N/A".to_string(),
                updated_at: "N/A".to_string(),
            },
        }
    }
    #[tokio::main]
    pub async fn get_assignment_info(url: String) -> Result<Assignment, Error> {
        let client = reqwest::Client::new();
        let assignment: Assignment = client
            .get(url)
            .timeout(Duration::from_secs(3))
            .send()
            .await?
            .json()
            .await?;
        Ok(assignment)
    }
}
/// Display impl for Assignment
impl fmt::Display for Assignment {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"Assignment Info:\nDescription: {description} \nID: {id} \nCreated at: {created} \nUpdated at: {updated} \nDue at: {due} \n",
        description = self.description,
        id = self.id,
        created = self.created_at,
        updated = self.updated_at,
        due = self.due_at)
    }
}

#[derive(Deserialize, Debug)]
pub struct Assignments(Vec<Assignment>);
/// Assignments impl. Parses JSON data for an entire classes assignments as text,
/// then uses the serde_json crate to parse it as a vector of assignments. Was much
/// more involved to write than the rest of the structs.
impl Assignments {
    pub async fn new(canvas_url: String, course_id: usize, api_token: String) -> Assignments {
        let url = format!(
            "{}api/v1/courses/{}/assignments?access_token={}",
            canvas_url, course_id, api_token
        );
        let assignments = Assignments::get_assignments(url).await;
        match assignments {
            Ok(_) => Assignments(assignments.unwrap()),
            Err(..) => Assignments(Vec::new()),
        }
    }
    /// get_assignments tries to parse a string of None Found if get_text returns an error.
    /// If given the time I would re-write this.
    async fn get_assignments(url: String) -> Result<Vec<Assignment>, serde_json::Error> {
        let text = Assignments::get_text(url);
        let rval = match text {
            Ok(_) => text.unwrap(),
            Err(..) => "None found".to_string(),
        };
        serde_json::from_str(&rval)
    }

    #[tokio::main]
    async fn get_text(url: String) -> Result<String, Error> {
        let json_data = reqwest::get(url).await?.text().await?;
        Ok(json_data)
    }

    pub fn get(&self) -> &Vec<Assignment> {
        &self.0
    }

    pub fn list(&self) {
        for i in 0..self.0.len() {
            println!("{}.   {}", i, self.0[i].description);
        }
    }
}
/// Display impl for Assignments. Since assignments is just a Vec,
/// this was much less straight forward than the other displays.
/// I found this guide and I followed along until I got a solution that I was
/// happy with. https://github.com/apolitical/impl-display-for-vec
impl fmt::Display for Assignments {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.iter().fold(Ok(()), |result, assignment| {
            result.and_then(|_| writeln!(f, "{}", assignment))
        })
    }
}

/// Tests verify valid endpoints. It's hardcoded for the rust class that we are currently in.
/// To get these to work, a token access number has to be added in the Canvas impl.
/// Since these are all async methods for testing, besides the default lib test, we have to use
/// the tokio::test library. Regular test does not support async functions.
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }

    #[tokio::test]
    async fn courses_test() -> Result<(), Error> {
        let canvas: Canvas = Canvas::new(None, None);
        let url = format!(
            "{}api/v1/courses/16412?access_token={}",
            canvas.url(),
            canvas.token()
        );
        let status = reqwest::get(url).await?.status();
        assert!(status.is_success());
        Ok(())
    }

    #[tokio::test]
    async fn assignment_test() -> Result<(), Error> {
        let canvas: Canvas = Canvas::new(None, None);
        let url = format!(
            "{}api/v1/courses/16412/assignments/188819?access_token={}",
            canvas.url(),
            canvas.token()
        );
        let status = reqwest::get(url).await?.status();
        assert!(status.is_success());
        Ok(())
    }

    #[tokio::test]
    async fn assignments_test() -> Result<(), Error> {
        let canvas: Canvas = Canvas::new(None, None);
        let url = format!(
            "{}api/v1/courses/16412/assignments?access_token={}",
            canvas.url(),
            canvas.token()
        );
        let status = reqwest::get(url).await?.status();
        assert!(status.is_success());
        Ok(())
    }
}
